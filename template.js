const puppeteer = require('puppeteer');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

const UsernameField = 'input[id=mat-input-0]';
const passwordField = 'input[id=mat-input-1]';
let environment = process.argv[2];
let websiteURL = 'https://nava-staging.syqlo.com/login';

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(websiteURL);


    if (environment === "dev" || environment === "staging" || environment === "prod")
    {
        readline.question('Enter NAVA username/E-Mail for specified environment:', (username) => {
            page.type(UsernameField, username);

            readline.question('Enter NAVA Password for specified environment:',  async (password)  => {
                page.type(passwordField, password);
                readline.close();
                await page.screenshot({path: 'screenshots/navaPic.jpg'});


                await Promise.all([
                    page.keyboard.press('Enter'),
                    page.waitForNavigation()

                ]);

                setTimeout(async () => {
                    await page.screenshot({path: 'screenshots/navaPic2.jpg'});
                    browser.close();
                }, 2000);



            });
        });
    }
    else {
        console.error('Error: Please specify your environment by passing a parameter. Valid options are: \'dev\', \'staging\', \'prod\' ');
        process.exit();
    }

})();








/*anmeldeButton.click();*/
